package com.idemon.springcloudserviceribbonhystrix;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class HelloService {

    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "handleError")
    public String hi(String name){
        return restTemplate.getForObject("http://service-hello/hello/" + name,String.class);
    }

    public String handleError(String name){
        return "Hi," + name + "! Sorry,An error occurred!";
    }
}
