package com.idemon.springcloudservicefeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient("service-hello")
public interface IHelloService {
    @RequestMapping("/hello/{name}")
    String sayHiService(@PathVariable("name") String name);
}
