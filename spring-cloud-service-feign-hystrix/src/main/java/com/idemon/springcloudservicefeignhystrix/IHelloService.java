package com.idemon.springcloudservicefeignhystrix;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value="service-hello",fallback = HelloServiceImpl4Error.class)
public interface IHelloService {
    @RequestMapping("/hello/{name}")
    String sayHiService(@PathVariable("name") String name);
}