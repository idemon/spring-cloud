package com.idemon.springcloudservicefeignhystrix;

import com.netflix.hystrix.contrib.metrics.eventstream.HystrixMetricsStreamServlet;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@EnableFeignClients
//@EnableCircuitBreaker
@EnableHystrix
@EnableHystrixDashboard
public class SpringCloudServiceFeignHystrixApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringCloudServiceFeignHystrixApplication.class, args);
	}

	/**
	 * 这里由于使用的spring-boot2.0，
	 * 需要添加 HystrixMetricsStreamServlet，
	 * 因为springboot的默认接收请求的路径不是 "/hystrix.stream"，
	 * 因此，在打开hystrix dashboard会报错：
	 * Unable to connect to Command Metric Stream；
	 * 只要在自己的项目里配置上HystrixMetricsStreamServlet就可以了;
	 * 或者在web.xml中添加该servlet配置也可
	 * @return
	 */
	@Bean
	public ServletRegistrationBean getServlet() {
		HystrixMetricsStreamServlet streamServlet = new HystrixMetricsStreamServlet();
		ServletRegistrationBean registrationBean = new ServletRegistrationBean(streamServlet);
		registrationBean.setLoadOnStartup(1);
		registrationBean.addUrlMappings("/hystrix.stream");
		registrationBean.setName("HystrixMetricsStreamServlet");
		return registrationBean;
	}
}
