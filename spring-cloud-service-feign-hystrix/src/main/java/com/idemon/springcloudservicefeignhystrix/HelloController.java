package com.idemon.springcloudservicefeignhystrix;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    @Autowired
    private IHelloService helloService;
    @RequestMapping("/hi/{name}")
    public String sayHi(@PathVariable("name") String name){
        return helloService.sayHiService(name);
    }
}
