package com.idemon.springcloudservicefeignhystrix;

import org.springframework.stereotype.Component;

@Component
public class HelloServiceImpl4Error implements  IHelloService{
    @Override
    public String sayHiService(String name) {
        return "Hi," + name + "! Sorry,An error occurred!";
    }
}
