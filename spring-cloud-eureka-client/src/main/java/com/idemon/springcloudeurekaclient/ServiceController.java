package com.idemon.springcloudeurekaclient;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ServiceController {
    @Value("${server.port}")
    private int port;
    @RequestMapping("/hello/{name}")
    public String home(@PathVariable("name") String name) {
        return "Hello, "+name + " The msg from " + port;
    }
}
